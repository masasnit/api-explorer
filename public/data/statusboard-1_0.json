{"endpoints": [{
    "name": "Publically Available Status Information",
    "methods": [{
        "MethodName": "All Services",
        "Synopsis": "Fetch a list of all current services tracked by this Statusboard instance.  No key is needed and should not be entered in the API Credentials box above.  Results are returned in JSON format.",
        "HTTPMethod": "GET",
        "URI": "/services/",
        "parameters": []
    }, {
        "MethodName": "Status Values",
        "Synopsis": "Fetch a list of all the possible status values for a service event.  No key is needed and should not be entered in the API Credentials box above.  Results are returned in JSON format.",
        "HTTPMethod": "GET",
        "URI": "/statuses/",
        "parameters": []
    }, {
        "MethodName": "Severity Values",
        "Synopsis": "Fetch a list of all the possible severity values for a service event.  No key is needed and should not be entered in the API Credentials box above.  Results are returned in JSON format.",
        "HTTPMethod": "GET",
        "URI": "/severities/",
        "parameters": []
    }, {
        "MethodName": "Individual Service Details",
        "Synopsis": "Fetch all available details regarding a service.  The service's slug value must be provided and forms part of the URL.  No key is needed and should not be entered in the API Credentials box above.  Results are returned in JSON format.",
        "HTTPMethod": "GET",
        "URI": "/services/:service_slug/",
        "parameters":[{
            "Name": "service_slug",
            "Required": "Y",
            "Default": "example",
            "Type": "string",
            "Description": "The full list of possible service slugs can be found by fetching the all services list.  Modify the default value provided here to review any available service."
        }]
    }, {
        "MethodName": "Individual Service Events List",
        "Synopsis": "Fetch all events for an individual service in reverse chronological order.  The service's slug value must be provided and forms part of the URL.  No key is needed and should not be entered in the API Credentials box above.  Results are returned in JSON format.",
        "HTTPMethod": "GET",
        "URI": "/services/:service_slug/events/",
        "parameters":[{
            "Name": "service_slug",
            "Required": "Y",
            "Default": "example",
            "Type": "string",
            "Description": "The full list of possible service slugs can be found by fetching the all services list."
        }, {
            "Name": "start",
            "Required": "N",
            "Default": "",
            "Type": "date",
            "Description": "Optionally filter events by time and show only those events that started after the provided date.  The date format must be YYYY-MM-DD"
        }]
    }]
}, {
    "name": "Data Source Management",
    "methods": [{
        "MethodName": "Update Service Description",
        "Synopsis": "Updates the service description for a data source.  The service's slug value must be provided and forms part of the URL.  The API key associated with this service is required and should be in the API Credentials box above.  The update result with the new description is returned in JSON format.  Contact the MASAS team for further information on connecting your data source to a Statusboard service.",
        "HTTPMethod": "POST",
        "URI": "/services/:service_slug/",
        "parameters":[{
            "Name": "service_slug",
            "Required": "Y",
            "Default": "",
            "Type": "string",
            "Description": "Services and their associated slug values are created and managed by the MASAS team."
        }],
        "bodyId": "sourceBody1",
        "bodyDefault": "{\"description\": \"New Description\"}",
        "bodySynopsis": "The new description for the service should be submitted in JSON format in the POST body."
    }, {
        "MethodName": "Heartbeat Update",
        "Synopsis": "Updates the last heartbeat time for a service, indicating that the data source is still functioning.  The service's slug value must be provided and forms part of the URL.  The API key associated with this service is required and should be in the API Credentials box above.",
        "HTTPMethod": "POST",
        "URI": "/services/:service_slug/heartbeat/",
        "parameters":[{
            "Name": "service_slug",
            "Required": "Y",
            "Default": "",
            "Type": "string",
            "Description": "Services and their associated slug values are created and managed by the MASAS team."
        }]
    }, {
        "MethodName": "Create Service Event",
        "Synopsis": "Creates a new event for a service, such as the data source going up or down.  The service's slug value must be provided and forms part of the URL.  The API key associated with this service is required and should be in the API Credentials box above.  The newly created event will be returned in JSON format.",
        "HTTPMethod": "POST",
        "URI": "/services/:service_slug/events/",
        "parameters":[{
            "Name": "service_slug",
            "Required": "Y",
            "Default": "",
            "Type": "string",
            "Description": "Services and their associated slug values are created and managed by the MASAS team."
        }],
        "bodyId": "sourceBody2",
        "bodyDefault": "{\"status\": \"up\", \"message\": \"Its Up\"}",
        "bodySynopsis": "The event information should be submitted in JSON format in the POST body.  The 'status' value must be a valid status slug found in the Statuses list.  The 'message' value should provide an explanation of the event."
    }]
}
]}

